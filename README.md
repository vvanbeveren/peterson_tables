# Peterson New Seismic Noise Model

Provides simple tables for Peterson's seismic background noise. Data obtain from original publication. See https://pubs.er.usgs.gov/publication/ofr93322 for information and original data.

The following units are used:

 * Period - In seconds
 * Frequency - In Hz
 * Displacement - m<sup>2</sup>/Hz \[dB\]
 * Velocity - (m/sec)<sup>2</sup>/Hz \[dB\]
 * Acceleration - (m/sec<sup>2</sup>)<sup>2</sup>/Hz \[dB\]

The following tables are provided:
 * nhnm.csv - New High Noise Model
 * nlnm.csv - New Low Noise Model

